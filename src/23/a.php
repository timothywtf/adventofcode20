<?php
$input = file_get_contents(__DIR__ . '/../input/23.txt');

$circle = [];
foreach (str_split($input) as $item) {
    $circle[] = (int)$item;
}

$currentCup = current($circle);
//echo implode(null, str_replace($currentCup, '(' . $currentCup . ')', $circle)) . PHP_EOL;

for ($j = 1; $j <= 100; $j++) {
    $prepared = false;
    while (!$prepared) {
        $shifted = array_shift($circle);
        if ($currentCup == $shifted) {
            array_unshift($circle, $shifted);
            $prepared = true;
        } else {
            array_push($circle, $shifted);
        }
    }

    $currentCup = array_shift($circle);
    $slice = array_splice($circle, 0, 3, null);

    $destinationCup = null;

    for ($i = $currentCup; $i > 0; $i--) {
        if (in_array($i, $circle)) {
            $destinationCup = $i;
            break;
        }
    }
    if (is_null($destinationCup)) {
        $destinationCup = max($circle);
    }
    array_unshift($circle, $currentCup);
    $key = array_search($destinationCup, $circle);
    $chunks = array_chunk($circle, $key+1);
    $chunkShift = array_shift($chunks);
    array_unshift($chunks, $slice);
    array_unshift($chunks, $chunkShift);
    $circle = call_user_func_array('array_merge', $chunks);
    reset($circle);
    $currentCup = next($circle);
//    echo implode(null, str_replace($currentCup, '(' . $currentCup . ')', $circle)) . PHP_EOL;
}

$checkCup = 1;
$check = false;
while (!$check) {
    $checkShifted = array_shift($circle);
    if ($checkCup == $checkShifted) {
        array_unshift($circle, $checkShifted);
        $check = true;
    } else {
        array_push($circle, $checkShifted);
    }
}

echo "Answer" . PHP_EOL;
array_shift($circle);
echo implode(null, $circle) . PHP_EOL;

