<?php
$input = explode(chr(10), file_get_contents(__DIR__ . '/../input/09.txt'));

$toMatch = 552655238;

for ($i = 0; $i < count($input); $i++) {
    $sum = (int)$input[$i];
    $terms = [(int)$input[$i]];
    $termTwoIndex = $i+1;
    while (true) {
        if ($sum > $toMatch) {
            break;
        }
        if (!isset($input[$termTwoIndex])) {
            var_dump("input does not exist");
            break;
        }

        $currentNumber = (int)$input[$termTwoIndex];
        $terms[] = $currentNumber;
        $sum += $currentNumber;
        $termTwoIndex++;

        if ($toMatch == $sum) {
            var_dump('found');
            var_dump(min($terms) + max($terms));
            break 2;
        }
    }
}