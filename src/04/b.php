<?php
$input = preg_split("#\n\s*\n#Uis", file_get_contents(__DIR__ . '/../input/04.txt'));

$validPassports = [];
foreach ($input as $passport) {
    $passportItems = preg_split('/[\s]+/', $passport);
    $properties = [];
    foreach ($passportItems as $passportItem) {
        list($key, $value) = explode(':', $passportItem);
        $properties[$key] = $value;
    }
    if (
        count($properties) == 8
        || (count($properties) == 7 && !isset($properties['cid']))
    ) {
        $validPassports[] = $properties;
    }
}

$validBirthYear = function ($birthYear) {
    return preg_match("/\d{4}/", $birthYear) && (int)$birthYear >= 1920 && (int)$birthYear <= 2002;
};

$validIssueYear = function ($issueYear) {
    return preg_match("/\d{4}/", $issueYear) && (int)$issueYear >= 2010 && (int)$issueYear <= 2020;
};

$validExpirationYear = function ($expirationYear) {
    return preg_match("/\d{4}/", $expirationYear) && (int)$expirationYear >= 2020 && (int)$expirationYear <= 2030;
};

$validHeight = function ($height) {
    if (preg_match("/(?<height>\d*)cm/", $height, $matches)) {
        return (int)$matches['height'] >= 150 && (int)$matches['height'] <= 193;
    } elseif (preg_match("/(?<height>\d*)in/", $height, $matches)) {
        return (int)$matches['height'] >= 59 && (int)$matches['height'] <= 76;
    }
    return false;
};

$validHairColor = function ($hairColor) {
    if (!preg_match("/\#(?<color_part>.{6})/", $hairColor, $matches)) {
        return false;
    }
    return (ctype_xdigit($matches['color_part']) && strlen($matches['color_part']) == 6);
};

$validEyeColor = function($eyeColor) {
    return in_array($eyeColor, ['amb','blu','brn','gry','grn','hzl','oth']);
};

$validPassportId = function ($passwordId) {
    return (bool)preg_match('/^\d{9}$/', $passwordId);
};

$countValidPassports = 0;
foreach ($validPassports as $key => $validPassport) {
    if (
        $validBirthYear($validPassport['byr'])
        && $validIssueYear($validPassport['iyr'])
        && $validExpirationYear($validPassport['eyr'])
        && $validHeight($validPassport['hgt'])
        && $validHairColor($validPassport['hcl'])
        && $validEyeColor($validPassport['ecl'])
        && $validPassportId($validPassport['pid'])
    ) {
        $countValidPassports++;
    }
}

var_dump($countValidPassports);
die;