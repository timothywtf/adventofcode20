<?php
$input = preg_split("#\n\s*\n#Uis", file_get_contents(__DIR__ . '/../input/04.txt'));

$validPassports = 0;
foreach ($input as $passport) {
    $passportItems = preg_split('/[\s]+/', $passport);
    $properties = [];
    foreach ($passportItems as $passportItem) {
        list($key, $value) = explode(':', $passportItem);
        $properties[$key] = $value;
    }
    if (
        count($properties) == 8
        || (count($properties) == 7 && !isset($properties['cid']))
    ) {
        $validPassports++;
    }
}

var_dump($validPassports);
die;