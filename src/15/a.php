<?php
$input = file_get_contents(__DIR__ . '/../input/15.txt');

$numbersAlreadySpoken = array_map('intval', explode(',', $input));
$previousNumber = $numbersAlreadySpoken[count($numbersAlreadySpoken) - 1];
$wasPreviousNumberFirst = true;

while (count($numbersAlreadySpoken) <= 2021) {
    if ($wasPreviousNumberFirst) {
        $currentNumber = 0;
    } else {
        $foundIndices = array_keys($numbersAlreadySpoken, $previousNumber);
        $firstTerm = array_pop($foundIndices);
        $secondTerm = array_pop($foundIndices);
        $currentNumber = $firstTerm - $secondTerm;
    }

    $wasPreviousNumberFirst = !in_array($currentNumber, $numbersAlreadySpoken);
    $numbersAlreadySpoken[] = $currentNumber;
    $previousNumber = $currentNumber;

    if ($count++ > 7){
        var_dump($numbersAlreadySpoken);
        die;

    }

}
var_dump($numbersAlreadySpoken[2019]);