<?php
$input = file_get_contents(__DIR__ . '/../input/15.txt');

$numbersAlreadySpoken = array_map('intval', explode(',', $input));
$previousNumber = $numbersAlreadySpoken[count($numbersAlreadySpoken) - 1];
$wasPreviousNumberFirst = true;

$findIndex = 30000000;
$zeroBasedIndex = $findIndex - 1;
$lastIndexRegister = array_flip($numbersAlreadySpoken);
$lastFoundDuplicate = null;

$count = count($numbersAlreadySpoken);
while (true) {
    if (is_null($lastFoundDuplicate)) {
        $currentNumber = 0;
    } else {
        $currentNumber = $count - 1 - $lastFoundDuplicate;
    }

    $lastFoundDuplicate = isset($lastIndexRegister[$currentNumber])
        ? $lastIndexRegister[$currentNumber]
        : null;
    $lastIndexRegister[$currentNumber] = $count;

    if ($count == $zeroBasedIndex) {
        var_dump($currentNumber);
        break;
    }
    $previousNumber = $currentNumber;
    $count++;
}