<?php
$input = explode(chr(10), file_get_contents(__DIR__ . '/../input/11.txt'));

$grid = [];
foreach ($input as $row) {
    $grid[] = str_split($row);
}

function getAdjacentSeatCount($x, $y, $grid) {
    $occupancy = [];
    for ($rowScan = -1; $rowScan <= 1; $rowScan++) {
        for ($columnScan = -1; $columnScan <= 1; $columnScan++) {
            if (0 == $rowScan && 0 == $columnScan) {
                continue;
            }
            if (!isset($grid[$x-$rowScan][$y-$columnScan])) {
                continue;
            }
            $occupancy[] = $grid[$x-$rowScan][$y-$columnScan];
        }
    }
    return $occupancy;
}

function countOccupiedSeats($grid)
{
    $occupiedSeats = 0;
    foreach ($grid as $row) {
        $countOccupancy = array_count_values($row);
        $occupiedSeats += (isset($countOccupancy['#']))
            ? $countOccupancy['#']
            : 0;
    }
    echo $occupiedSeats . ' occupied';
}

function occupy($x, $y, &$transformedGrid, $grid) {
    $occupancy = getAdjacentSeatCount($x, $y, $grid);
    $countOccupancy = array_count_values($occupancy);
    if (!isset($countOccupancy['#']) && $grid[$x][$y] == 'L') {
        $transformedGrid[$x][$y] = '#';
    }
}

function freeUp($x, $y, &$transformedGrid, $grid) {
    $occupancy = getAdjacentSeatCount($x, $y, $grid);
    $countOccupancy = array_count_values($occupancy);
    if (isset($countOccupancy['#']) && $countOccupancy['#'] >= 4 && $grid[$x][$y] == '#') {
        $transformedGrid[$x][$y] = 'L';
    }
}

function paint($grid)
{
    for ($x = 0; $x < count($grid); $x++) {
        for ($y = 0; $y < count($grid[0]); $y++) {
            echo $grid[$x][$y];
        }
        echo PHP_EOL;
    }
    return $grid;
}

function gridState($grid) {
    $state = '';
    foreach ($grid as $row) {
        $state .= implode('', $row);
    }
    return md5($state);
}

function transform ($count, $grid) {
    $transformedGrid = $grid;

    for ($x = 0; $x < count($grid); $x++) {
        for ($y = 0; $y < count($grid[0]); $y++) {
            if ($count % 2 == 0) {
                occupy($x, $y, $transformedGrid, $grid);
            } else {
                freeUp($x, $y, $transformedGrid , $grid);
            }
        }
    }
    return $transformedGrid;
}

$count = 0;
$duplicateFound = false;
$states = [gridState($grid)];

paint($grid);
while (!$duplicateFound) {
    $grid = transform($count++, $grid);
    echo PHP_EOL;
    paint($grid);

    $gridState = gridState($grid);
    if (in_array($gridState, $states)) {
        countOccupiedSeats($grid);
        break;
    } else {
        $states[] = $gridState;
    }
}