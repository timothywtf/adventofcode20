<?php
$input = explode(chr(10), file_get_contents(__DIR__ . '/../input/08.txt'));

$register = [];
foreach ($input as $item) {
    preg_match("/^(?<op>[a-z]{3}) (?<sign>[+-]){1}(?<count>\d*)$/", $item, $matches);
    $register[] = [
        'count' => $matches['count'],
        'sign' => $matches['sign'],
        'op' => $matches['op']
    ];
}

$pointer = 0;
$operationHistory = [];
$accumulator = 0;

while (true) {
    $currentOperation = $register[$pointer];

    if (in_array($pointer, $operationHistory)) {
        var_dump($accumulator);
        break;
    } else {
        $operationHistory[] = $pointer;
    }
    switch ($currentOperation['op']) {
        case "jmp":
            if ($currentOperation['sign'] == '+') {
                $pointer += $currentOperation['count'];
            } else {
                $pointer -= $currentOperation['count'];
            }
            break;
        case "acc":
            if ($currentOperation['sign'] == '+') {
                $accumulator += $currentOperation['count'];
            } else {
                $accumulator -= $currentOperation['count'];
            }
            $pointer++;
            break;
        case "nop":
            $pointer++;
            break;
    }
}
