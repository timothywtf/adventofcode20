<?php
$input = explode(chr(10), file_get_contents(__DIR__ . '/../input/08.txt'));

$register = [];
foreach ($input as $item) {
    preg_match("/^(?<op>[a-z]{3}) (?<sign>[+-]){1}(?<count>\d*)$/", $item, $matches);
    $register[] = [
        'count' => $matches['count'],
        'sign' => $matches['sign'],
        'op' => $matches['op']
    ];
}

/**
 * @param $register
 * @return bool
 * @throws Exception
 */
function runCode ($register) {
    $pointer = 0;
    $operationHistory = [];
    $accumulator = 0;

    while (true) {
        if ($pointer + 1 > count($register)) {
            var_dump("terminated");
            var_dump($accumulator);
            return true;
        }
        $currentOperation = $register[$pointer];

        if (in_array($pointer, $operationHistory)) {
            return false;
        } else {
            $operationHistory[] = $pointer;
        }
        switch ($currentOperation['op']) {
            case "jmp":
                if ($currentOperation['sign'] == '+') {
                    $pointer += $currentOperation['count'];
                } else {
                    $pointer -= $currentOperation['count'];
                }
                break;
            case "acc":
                if ($currentOperation['sign'] == '+') {
                    $accumulator += $currentOperation['count'];
                } else {
                    $accumulator -= $currentOperation['count'];
                }
                $pointer++;
                break;
            case "nop":
                $pointer++;
                break;
        }
    }

    throw new Exception("something went wrong");
}

foreach ($register as $key => $item) {
    $alteredRegister = $register;
    if ($alteredRegister[$key]['op'] == 'nop') {
        $alteredRegister[$key]['op'] = 'jmp';
    } elseif ($alteredRegister[$key]['op'] == 'jmp') {
        $alteredRegister[$key]['op'] = 'nop';
    }

    try {
        if (runCode($alteredRegister)) {
            break;
        }
    } catch (Exception $e) {
        var_dump($e->getMessage());
    }
}

