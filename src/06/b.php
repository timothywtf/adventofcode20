<?php
$input = preg_split("#\n\s*\n#Uis", file_get_contents(__DIR__ . '/../input/06.txt'));

$total = 0;
foreach ($input as $item) {
    $persons = explode(PHP_EOL, $item);
    $charList = array_unique(array_filter(array_map('trim', str_split($item))));
    foreach ($charList as $character) {
        $charCount = 0;
        foreach ($persons as $person) {
            if (in_array($character, str_split($person))) {
                $charCount++;
            }
        }
        if ($charCount == count($persons)) {
            $total++;
        }
    }
}
var_dump($total);
