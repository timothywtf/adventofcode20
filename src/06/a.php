<?php
$input = preg_split("#\n\s*\n#Uis", file_get_contents(__DIR__ . '/../input/06.txt'));

$total = 0;
foreach ($input as $item) {
    $chars = array_filter(array_map('trim', str_split($item)));
    $total += count(array_count_values($chars));
}
var_dump($total);
