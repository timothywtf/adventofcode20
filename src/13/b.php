<?php
$input = file_get_contents(__DIR__ . '/../input/13.txt');
$input = explode(chr(10), $input);

$buses = explode(",",$input[1]);
$n = 0;
$inc = (int)$buses[0];

for($t=1;$t<sizeof($buses);$t++){
    if($buses[$t] == "x") continue;
    $first = 0;
    while(true) {
        $bus = (int)$buses[$t];
        if(floor(($n+$t)/$bus) == ($n+$t)/$bus) {
            echo "{$t}|{$bus}|" . floor(($n+$t)/$bus) . PHP_EOL;
            if($first == 0) {
                if($t == sizeof($buses) -1) {
                    //final entry - STOP
                    echo $n;
                    exit;
                }
                $first = $n;
            }
            else {
                $inc = $n - $first;
                break;
            }
        }
        $n+=$inc;
    }
}