<?php
$input = file_get_contents(__DIR__ . '/../input/13.txt');
$input = explode(chr(10), $input);
$timeStamp = (int)$input[0];
$busses = explode(',', $input[1]);
$busses = array_filter($busses, function ($bus) {
    return $bus != 'x';
});
$busDiffs = [];

foreach ($busses as $bus) {
    $latestTime = 0;

    while ($latestTime < $timeStamp) {
        $nextTime =  $latestTime + (int)$bus;
        if ($nextTime >= $timeStamp) {
            $diff = $timeStamp - $nextTime;
            $busDiffs[$bus] = [
                'diff' => abs($diff),
                'answer' => abs($diff) * $bus
            ];
            break;
        }
        $latestTime += $bus;
    }
}

var_dump($busDiffs);