<?php
$input = file_get_contents(__DIR__ . '/../input/16.txt');
$input = preg_split("#\n\s*\n#Uis", $input);

$words = explode(PHP_EOL, $input[0]);
$rules = [];
foreach ($words as $word) {
    if (preg_match("/(?<word>[ a-z]*): (?<term1a>\d*)-(?<term1b>\d*) or (?<term2a>\d*)-(?<term2b>\d*)/", $word, $matches)) {
        $rules[$matches['word']] = [
            'term1' => [ $matches['term1a'], $matches['term1b']],
            'term2' => [ $matches['term2a'], $matches['term2b']],

        ];
    }
}

$myTicket = explode(PHP_EOL, $input[1]);
array_shift($myTicket);

$nearbyTickets = explode(PHP_EOL, $input[2]);
array_shift($nearbyTickets);

$positions = [];

foreach ($nearbyTickets as $ticketKey => $nearbyTicket) {
    $ticketNumbers = explode(",", $nearbyTicket);
    $errorNumbers = array_filter($ticketNumbers, function ($ticketNumber) use ($rules) {
        foreach ($rules as $rule) {
            if (
                in_array($ticketNumber, range($rule['term1'][0], $rule['term1'][1]))
                || in_array($ticketNumber, range($rule['term2'][0], $rule['term2'][1]))
            ) {
                return false;
            }

        }
        return true;
    });
    $errorNumbers = array_map('intval', array_filter($errorNumbers));
    if (!empty($errorNumbers)) {
        unset($nearbyTickets[$ticketKey]);
    }
}

foreach ($nearbyTickets as $ticketKey => $nearbyTicket) {
    $ticketNumbers = explode(",", $nearbyTicket);
    foreach ($ticketNumbers as $columnNumber => $ticketNumber) {
        $positions[$columnNumber][] = $ticketNumber;
    }
}

function getSinglePossibility($numbers, $rules)
{
    $rulesCopy = $rules;
    foreach ($rulesCopy as $ruleIndex => $rule) {
        foreach ($numbers as $word => $number) {
            if (
                !in_array((int)$number, range((int)$rule['term1'][0], (int)$rule['term1'][1]))
                && !in_array((int)$number, range((int)$rule['term2'][0], (int)$rule['term2'][1]))
            ) {
                unset($rulesCopy[$ruleIndex]);
            }
        }
    }


    return (count($rulesCopy) === 1)
        ? key($rulesCopy)
        : false;
}

$resultPositions = [];
while (!empty($rules)) {
    foreach ($positions as $positionKey => $position) {
        $singlePosition = getSinglePossibility($positions[$positionKey], $rules);
        if (false !== $singlePosition) {
            $resultPositions[$positionKey] = $singlePosition;
            unset($positions[$positionKey]);
            unset($rules[$singlePosition]);
            break;
        }
    }
}

$departures = array_keys(array_filter($resultPositions, function ($position) {
    return stristr($position, 'departure');
}));

$answers = [];
$myTicketNumbers = explode(",", $myTicket[0]);
foreach ($departures as $departure) {
    $answers[] = $myTicketNumbers[$departure];
}

var_dump(array_product($answers));
