<?php
$input = file_get_contents(__DIR__ . '/../input/16.txt');
$input = preg_split("#\n\s*\n#Uis", $input);

$words = explode(PHP_EOL, $input[0]);
$rules = [];
foreach ($words as $word) {
    if (preg_match("/(?<word>[ a-z]*): (?<term1a>\d*)-(?<term1b>\d*) or (?<term2a>\d*)-(?<term2b>\d*)/", $word, $matches)) {
        $rules[$matches['word']] = [
            'term1' => [ $matches['term1a'], $matches['term1b']],
            'term2' => [ $matches['term2a'], $matches['term2b']],

        ];
    }
}

$nearbyTickets = explode(PHP_EOL, $input[2]);
array_shift($nearbyTickets);

$errorCount = 0;
foreach ($nearbyTickets as $nearbyTicket) {
    $ticketNumbers = explode(",", $nearbyTicket);
    $errorNumbers = array_filter($ticketNumbers, function ($ticketNumber) use ($rules) {
        foreach ($rules as $rule) {
            if (
                in_array($ticketNumber, range($rule['term1'][0], $rule['term1'][1]))
                || in_array($ticketNumber, range($rule['term2'][0], $rule['term2'][1]))
            ) {
                return false;
            }

        }
        return true;
    });
    $errorNumbers = array_map('intval', array_filter($errorNumbers));
    $errorCount += array_sum($errorNumbers);
}
var_dump($errorCount);

