<?php
$input = explode(chr(10), file_get_contents(__DIR__ . '/../input/10.txt'));
natsort($input);
$input = array_values($input);
$input = array_map('intval', $input);
array_unshift($input, 0);
array_push($input, max($input) + 3);

$chunks = [];
$chunk = [];
$lastItem = null;
foreach ($input as $item) {
    if (is_null($lastItem) || $item == $lastItem + 1) {
        $chunk[] = $item;
    } else {
        $chunks[] = $chunk;
        $chunk = [$item];
    }
    $lastItem = $item;
}

$combinations = [];
foreach ($chunks as $chunk) {
    $count = count($chunk);
    switch ($count) {
        case 1: $combinations[] = 1; break;
        case 2: $combinations[] = 1; break;
        case 3: $combinations[] = 2; break;
        case 4: $combinations[] = 4; break;
        case 5: $combinations[] = 7; break;
    }
}

var_dump(array_product($combinations));