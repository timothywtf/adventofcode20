<?php
$input = explode(chr(10), file_get_contents(__DIR__ . '/../input/10.txt'));

natsort($input);

$startJoltage = 0;
$lastJoltage = null;

$delta = [];
foreach ($input as $adapter) {
    $delta[] = is_null($lastJoltage) ? $adapter : $adapter - $lastJoltage;
    $lastJoltage = $adapter;
}
$distribution = array_count_values($delta);
//built in adapter
$distribution[3]++;
var_dump($distribution[1] * $distribution[3]);
