<?php

namespace App\Library;

/**
 * Class Matrix
 * @package App\Library
 */
class Matrix extends \Matrix\Matrix
{
    public $id;

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId($idOnly = false)
    {
        if ($idOnly) {
            preg_match("/^(?<id>\d*)\s.*$/", $this->id, $matches);
            if ($matches['id']) {
                return $matches['id'];
            }
        }
        return $this->id;
    }

    /**
     * @param Matrix $matrix
     * @return Matrix
     */
    public static function rotate(Matrix $matrix)
    {
        $rotatedData = [];
        foreach ($matrix->columns() as $column) {
            $rotatedData[] = array_reverse(array_map('array_pop', $column->toArray()));
        }

        return new self($rotatedData);
    }

    /**
     * @param Matrix $matrix
     * @return Matrix
     */
    public static function flip(Matrix $matrix)
    {
        $flippedData = [];

        foreach ($matrix->rows() as $row) {
            $flippedData[] = array_reverse($row->toArray()[0]);
        }

        return new self($flippedData);
    }


    public function printTile()
    {
        if ($this->id) {
            echo "Tile " . $this->id . ':' . PHP_EOL;
        }
        echo $this->output();
        echo chr(10);
    }

    public function output()
    {
        $output = '';
        foreach ($this->rows() as $row) {
            foreach ($row->columns() as $column) {
                $output .= $column;
            }
            $output .= PHP_EOL;
        }
        return trim($output);
    }

    public function getEdge($side)
    {
        $edge = '';
        switch ($side) {
            case "E":
                foreach ($this->rows() as $row) {
                    $edge .= array_pop($row->toArray()[0]);
                };
            break;
            case "S":
                $edge = $this->getRows(10)->output();
            break;
            case "W":
                foreach ($this->rows() as $row) {
                    $edge .= array_shift($row->toArray()[0]);
                };
            break;
            case "N":
                $edge = $this->getRows(1)->output();
            break;
        }
        return $edge;
    }

    /**
     * @param $register
     * @return array
     */
    function getTileOptions()
    {
        $register[] = $this;
        //rotate 90
        $rotate90 = self::rotate($this);
        $rotate90->setId($this->getId() . " +90");
        $register[] = $rotate90;
        //rotate 180
        $rotate180 = self::rotate($rotate90);
        $rotate180->setId($this->getId() . " +180");
        $register[] = $rotate180;
        //rotate 270
        $rotate270 = self::rotate($rotate180);
        $rotate270->setId($this->getId() . " +270");
        $register[] = $rotate270;

        //flip 0
        $flip0 = self::flip($this);
        $flip0->setId($this->getId() . " (flipped)");
        $register[] = $flip0;
        //flip 90
        $flip90 = self::rotate($flip0);
        $flip90->setId($this->getId() . " (flipped) +90");
        $register[] = $flip90;
        //flip 180
        $flip180 = self::rotate($flip90);
        $flip180->setId($this->getId() . " (flipped) +180");
        $register[] = $flip180;
        //flip 270
        $flip270 = self::rotate($flip180);
        $flip270->setId($this->getId() . " (flipped) +270");
        $register[] = $flip270;
        return $register;
    }

    public function getInnerFrame()
    {
        $innerFrame = $this->grid;
        array_pop($innerFrame);
        array_shift($innerFrame);
        foreach ($innerFrame as $key => $item) {
            array_pop($item);
            array_shift($item);
            $innerFrame[$key] = $item;
        }
        return new self($innerFrame);
    }

    /**
     * @return array
     */
    public function getGrid()
    {
        return $this->grid;
    }

}