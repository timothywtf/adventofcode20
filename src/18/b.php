<?php
$input = file_get_contents(__DIR__ . '/../input/18.txt');
$input = explode(chr(10), $input);

$sums = [];
function runNumbers(&$characters)
{
    $subSum = null;
    $lastOperator = null;
    while (!empty($characters)) {
        $currentPart = array_shift($characters);
        if (in_array($currentPart, ["+", "*"])) {
            $lastOperator = $currentPart;
        } elseif ($currentPart == "(") {
            array_unshift($characters, runNumbers($characters));
        } elseif ($currentPart == ")") {
            break;
        } else {
            $subSum = ($lastOperator == '+')
                ? (is_null($subSum) ? 0 : $subSum) + $currentPart
                : (is_null($subSum) ? 1 : $subSum) * $currentPart
            ;
        }

    }
    return $subSum;
}

function addParentheses(&$parts)
{
    $plusPositions = array_keys($parts, "+");

    for ($i = 0; $i < count($plusPositions); $i++){
        $plusPositions = array_keys($parts, "+");
        $currentPlus = $plusPositions[$i];
        $skipParCount = 0;
        for ($l = $currentPlus-1; $l >= 0; $l--) {
            if ($parts[$l] == ')') {
                $skipParCount++;
            } else if ($parts[$l] == '(') {
                $skipParCount--;
            }
            if ($skipParCount === 0) {
                $parts[$l] = "(" . $parts[$l];
                $parts = array_values(str_split(implode("", $parts)));
                break;
            }
        }

        $plusPositions = array_keys($parts, "+");
        $currentPlus = $plusPositions[$i];
        $skipParCount = 0;
        for ($r = $currentPlus+1; $r < count($parts); $r++) {
            if ($parts[$r] == '(') {
                $skipParCount++;
            } else if ($parts[$r] == ')') {
                $skipParCount--;
            }
            if ($skipParCount === 0) {
                $parts[$r] = $parts[$r] . ")";
                $parts = array_values(str_split(implode("", $parts)));
                break;
            }
        }
    }
}


foreach ($input as $row) {
    $parts = array_values(array_filter(str_split($row), function ($item) {
        return $item != ' ';
    }));
    addParentheses($parts);
    $sums[] = runNumbers($parts);
}
var_dump(array_sum($sums));
