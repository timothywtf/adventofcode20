<?php
$input = file_get_contents(__DIR__ . '/../input/18.txt');
$input = explode(chr(10), $input);

$sums = [];
function runNumbers(&$characters)
{
    $subSum = null;
    $lastOperator = null;
    while (!empty($characters)) {
        $currentPart = array_shift($characters);
        if ($currentPart == ' ') {
            continue;
        }
        if (in_array($currentPart, ["+", "*"])) {
            $lastOperator = $currentPart;
        } elseif ($currentPart == "(") {
            array_unshift($characters, runNumbers($characters));
        } elseif ($currentPart == ")") {
            break;
        } else {
            $subSum = ($lastOperator == '+')
                ? (is_null($subSum) ? 0 : $subSum) + $currentPart
                : (is_null($subSum) ? 1 : $subSum) * $currentPart
            ;
        }

    }
    return $subSum;
}

foreach ($input as $item) {
    $parts = str_split($item);
    $sums[] = runNumbers($parts);
}
var_dump(array_sum($sums));
