<?php
$input = file_get_contents(__DIR__ . "/../input/19.txt");

list($rules, $messages) = preg_split("#\n\s*\n#Uis", $input);
$rules = explode(chr(10), $rules);
$messages = explode(chr(10), $messages);

$ruleRegister = [];
foreach ($rules as $rule) {
    if (preg_match('/(?<ruleId>\d*): "?(?<ruleContent>.*)"?/', $rule, $matches)) {
        $ruleRegister[$matches['ruleId']] = trim($matches['ruleContent'], " \t\n\r\0\x0B\"");
    }
}
ksort($ruleRegister);

$ruleRegister[8] = '42 +';
$ruleRegister[11] = '42 (?: 42 (?: 42 (?: 42 31 )? 31 )? 31 )? 31';

$okToReplace = [];
foreach ($ruleRegister as $ruleId => $rule) {
    if (preg_match("/^(a|b){1}$/", $rule)) {
        $okToReplace[$ruleId] = $ruleId;
    }
}

$keepTrying = true;
while ($keepTrying) {

    foreach ($ruleRegister as $ruleId => &$rule) {
        if (preg_match_all("/\d*/", $rule, $matches)) {
            $foundIntegers = array_filter($matches[0]);
            if (empty($foundIntegers)) {
                continue;
            }
            $replaceableCount = count(array_filter(array_values($foundIntegers), function ($foundInteger) use ($okToReplace) {
                return (in_array($foundInteger, $okToReplace));
            }));
            $allReplaceable = $replaceableCount == count($foundIntegers);

            if($allReplaceable) {
                foreach (array_unique($foundIntegers) as $foundInteger) {
                    $replacement = $ruleRegister[$foundInteger];
                    $replacement = (preg_match("/[|]+/", $replacement))
                        ? "(" . $replacement . ")"
                        : $replacement;

                    $rule = preg_replace(
                        '/(?<=\s|^)'.$foundInteger.'(?=\s|$)/',
                        $replacement,
                        $rule
                    );
                }
                $okToReplace[$ruleId] = $ruleId;
            }
        }
    }

    $keepTrying = count($okToReplace) != count($ruleRegister);
}

$ruleRegister = array_map(function ($rule) {
    return str_replace(" ", "", $rule);
}, $ruleRegister);

$count = 0;
foreach ($messages as $message) {
    if (preg_match("/^" . $ruleRegister[0] . "$/", $message)) {
        echo $message . PHP_EOL;
        $count++;
    }
}
var_dump($count);