<?php
$input = file_get_contents(__DIR__ . "/../input/19.txt");

list($rules, $messages) = preg_split("#\n\s*\n#Uis", $input);
$rules = explode(chr(10), $rules);
$messages = explode(chr(10), $messages);

$ruleRegister = [];
foreach ($rules as $rule) {
    if (preg_match('/(?<ruleId>\d*): "?(?<ruleContent>.*)"?/', $rule, $matches)) {
        $ruleRegister[$matches['ruleId']] = trim($matches['ruleContent'], " \t\n\r\0\x0B\"");
    }
}

$test = 0;
$diff = true;
while ($diff) {
    $letterOnlyRules = array_filter($ruleRegister, function ($rule) {
        return preg_match("/^([^0-9]*)$/", $rule);
    }, ARRAY_FILTER_USE_BOTH);
    $diff = count($ruleRegister) !== count($letterOnlyRules);

    foreach ($ruleRegister as $ruleId => $rule) {
        foreach ($letterOnlyRules as $letterOnlyKey => $letterOnlyRule) {
            $var = $ruleRegister[$ruleId] ?? $rule;
            $var = " " . $var . " ";
            $ruleRegister[$ruleId] = str_replace(" " . $letterOnlyKey . " ", ' - ' . $letterOnlyRule . ' - ', $var);
        }
        $ruleRegister[$ruleId] = sprintf("(%s)", $ruleRegister[$ruleId]);
    }
}

$totalMatch = str_replace(" ", "", $ruleRegister[0]);
$totalMatch = str_replace("-", "", $totalMatch);
$matchedMessages = 0;
foreach ($messages as $message) {
    if (preg_match("/^" . $totalMatch . "$/", $message)) {
        $matchedMessages++;
    }
}
var_dump($matchedMessages);
die;