<?php
$input = file_get_contents(__DIR__ . '/../input/22.txt');
$input = preg_split("#\n\s*\n#Uis", $input);

$p1 = explode(chr(10), $input[0]);
array_shift($p1);
array_map('intval', $p1);
$p2 = explode(chr(10), $input[1]);
array_shift($p2);
array_map('intval', $p2);


while (!empty($p1) && !empty($p2)) {
    $g1 = array_shift($p1);
    $g2 = array_shift($p2);
    if ($g1 > $g2) {
        array_push($p1, $g1);
        array_push($p1, $g2);
    } else {
        array_push($p2, $g2);
        array_push($p2, $g1);
    }
}

$winner = (empty($p2)) ? $p1 : $p2;
$score = 0;
if (!empty($winner)) {
    for ($i = count($winner) - 1; $i >= 0; $i--) {
        $score += ((count($winner) - $i) * $winner[$i]);
    }
}
var_dump($winner);
var_dump($score);

