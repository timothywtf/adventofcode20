<?php
$input = file_get_contents(__DIR__ . '/../input/22.txt');

$input = preg_split("#\n\s*\n#Uis", $input);

$p1 = explode(chr(10), $input[0]);
array_shift($p1);
array_map('intval', $p1);
$p2 = explode(chr(10), $input[1]);
array_shift($p2);
array_map('intval', $p2);

$states = [];

function playGame($p1, $p2, &$states, $gameCount = 1) {
    $out = false;
    if ($out) {
        echo chr(10) . "=== Game $gameCount ===" . PHP_EOL . chr(10);
    }

    $roundCount = 1;
    while (!empty($p1) && !empty($p2)) {
        if ($out) {
            echo "-- Round $roundCount (Game $gameCount) --" . PHP_EOL;
        }
        $state = implode('-', $p1) . ':' . implode('-', $p2);

        if (!isset($states[$gameCount])) {
            $states[$gameCount] = [];
        }
        if (!isset($states[$gameCount][$state])) {
            $states[$gameCount][$state] = $state;
        } else {
            if ($out) {
                echo "double state!!!!";
            }
            $p2 = [];
            break;
        }
        if ($out) {
            echo "Player 1's deck: " . implode(", ", $p1) . PHP_EOL;
            echo "Player 2's deck: " . implode(", ", $p2) . PHP_EOL;
        }
        $g1 = array_shift($p1);
        $p1ShouldRecurse = count($p1) >= $g1;
        $g2 = array_shift($p2);
        $p2ShouldRecurse = count($p2) >= $g2;
        if ($out) {
            echo "Player 1 plays: $g1" . PHP_EOL;
            echo "Player 2 plays: $g2" . PHP_EOL;
        }

        if ($p1ShouldRecurse && $p2ShouldRecurse) {
            if ($out) {
                echo "Playing a sub-game to determine the winner..." . PHP_EOL;
            }
            $p1Copy = $p1;

            $p1Copy = array_slice($p1Copy, 0, $g1);
            $p2Copy = $p2;
            $p2Copy = array_slice($p2Copy, 0, $g2);

            $subGameResult = playGame($p1Copy, $p2Copy, $states, $gameCount + 1);
            if ($subGameResult['winner'] == 'p1') {
                if ($out) {
                    echo "Player 1 wins round $roundCount of game $gameCount!" . PHP_EOL . chr(10);
                }

                array_push($p1, $g1);
                array_push($p1, $g2);
            } else {
                if ($out) {
                    echo "Player 2 wins round $roundCount of game $gameCount!" . PHP_EOL . chr(10);
                }
                array_push($p2, $g2);
                array_push($p2, $g1);
            }
            $roundCount++;
            continue;
        }

        if ($g1 > $g2) {
            if ($out) {
                echo "Player 1 wins round $roundCount of game $gameCount!" . PHP_EOL . chr(10);
            }
            array_push($p1, $g1);
            array_push($p1, $g2);
        } else {
            if ($out) {
                echo "Player 2 wins round $roundCount of game $gameCount!" . PHP_EOL . chr(10);
            }
            array_push($p2, $g2);
            array_push($p2, $g1);
        }
        $roundCount++;
    }

    $winner = (empty($p1)) ? 'p2' : 'p1';
    if ($out) {
        echo "The winner of game $gameCount is $winner" . PHP_EOL . chr(10);
    }
    if ($out) {
        echo "...anyway, back to game " . ($gameCount - 1) . PHP_EOL;
    }
    return [
        'winner' => $winner,
        'p1' => $p1,
        'p2' => $p2
    ];
}


$result = playGame($p1, $p2, $states);
echo chr(10) . "== Post-game results ==" . PHP_EOL;
echo "Player 1's deck: " . implode(", ", $result['p1']) . PHP_EOL;
echo "Player 2's deck: " . implode(", ", $result['p2']) . PHP_EOL;

$winner = (empty($result['p2'])) ? $result['p1'] : $result['p2'];
$score = 0;
if (!empty($winner)) {
    for ($i = count($winner) - 1; $i >= 0; $i--) {
        $score += ((count($winner) - $i) * $winner[$i]);
    }
}
echo "Score: " . $score;


