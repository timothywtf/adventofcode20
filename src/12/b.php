<?php
$input = explode(chr(10), file_get_contents(__DIR__ . '/../input/12.txt'));

$positionX = 0;
$positionY = 0;
$waypointPositionX = 10;
$waypointPositionY = -1;

foreach ($input as $instruction) {
    var_dump($instruction);
    if (preg_match('/(?<ins>[A-Z]{1})(?<count>\d*)/', $instruction, $matches)) {
        $ins = $matches['ins'];
        $count = (int)trim($matches['count']);
        if ($ins == null || $count == null) {
            throw new \Exception("something went wrong");
        }

        if (in_array($ins, ["L", "R"])) {
            $degrees = (int)$count;
            for ($t = 1; $t <= $degrees / 90; $t++) {
                $xDirection = $positionX - $waypointPositionX;
                $yDirection = $positionY - $waypointPositionY;
                $deltaCoords = [$xDirection, $yDirection];
                $newDeltaCoords = $deltaCoords;
                switch ($ins) {
                    case 'R':
                        $newDeltaCoords[0] = $deltaCoords[1];
                        $newDeltaCoords[1] = $deltaCoords[0] * -1;
                        break;
                    case 'L':
                        $newDeltaCoords[1] = $deltaCoords[0];
                        $newDeltaCoords[0] = $deltaCoords[1] * -1;
                        break;
                }
                $waypointPositionX = $positionX + $newDeltaCoords[0];
                $waypointPositionY = $positionY + $newDeltaCoords[1];
            }
        } else if ($ins == 'F') {
            $xDiff = $waypointPositionX - $positionX;
            $yDiff = $waypointPositionY - $positionY;
            $positionX += ($xDiff * $count);
            $waypointPositionX = $positionX + $xDiff;
            $positionY += ($yDiff * $count);
            $waypointPositionY = $positionY + $yDiff;
        } else {
            switch ($ins) {
                case "N":
                    $waypointPositionY -= $count;
                    break;
                case "S":
                    $waypointPositionY += $count;
                    break;
                case "E":
                    $waypointPositionX += $count;
                    break;
                case "W":
                    $waypointPositionX -= $count;
                    break;
            }
        }
    }
}

$xmanhattan = abs($positionX);
$ymanhattan = abs($positionY);
var_dump($xmanhattan + $ymanhattan);