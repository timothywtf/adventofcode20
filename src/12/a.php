<?php
$input = explode(chr(10), file_get_contents(__DIR__ . '/../input/12.txt'));
$eastWestPosition = 0;
$northSouthPosition = 0;

$position = [0,0];
$direction = "E";
$directions = ["N", "E", "S", 'W'];

function turn($turnDirection, &$direction, $directions) {
    switch ($turnDirection) {
        case 'L':
            $updatedDirections = $directions;
            array_unshift($updatedDirections, array_pop($updatedDirections));
            $direction = $updatedDirections[array_search($direction, $directions)];
        break;
        case 'R':
            $updatedDirections = $directions;
            array_push($updatedDirections, array_shift($updatedDirections));
            $direction = $updatedDirections[array_search($direction, $directions)];
        break;
    }
}
function move($dir, $count, &$position)
{
    switch ($dir) {
        case "N":
            $position[0] -= $count;
            break;
        case "S":
            $position[0] += $count;
            break;
        case "E":
            $position[1] += $count;
            break;
        case "W":
            $position[1] -= $count;
            break;
    }
}
foreach ($input as $instruction) {
    if (preg_match('/(?<ins>[A-Z]{1})(?<count>\d*)/', $instruction, $matches)) {
        $ins = $matches['ins'];
        $count = $matches['count'];
        if ($ins == null || $count == null) {
            throw new \Exception("something went wrong");
        }

        if (in_array($ins, ["L", "R"])) {
            $degrees = (int)$count;
            for ($t = 1; $t <= $degrees/90; $t++) {
                turn($ins, $direction, $directions);
            }
        } else {
            move(($ins == 'F') ? $direction : $ins, $count, $position);
        }
    }
}

var_dump(abs($position[0]) + abs($position[1]));