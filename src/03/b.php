<?php
$input = explode(chr(10), file_get_contents(__DIR__ . '/../input/03.txt'));

function slope($right, $down, $input) {
    $treeCount = 0;
    $column = 0;
    for ($i = 0; $i <= count($input) - 1; $i += $down) {
        if ($input[$i][$column] == '#') {
            $treeCount++;
        }
        $column += $right;
        if ($column > (strlen($input[$i]) - 1)) {
            $column -= strlen($input[$i]);
        }
    }

    return $treeCount;
}

$multiplied = slope(1, 1, $input) * slope(3, 1, $input) * slope(5, 1, $input) * slope(7, 1, $input) * slope(1, 2, $input);
var_dump($multiplied);