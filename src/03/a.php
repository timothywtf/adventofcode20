<?php
$input = explode(chr(10), file_get_contents(__DIR__ . '/../input/03.txt'));

$treeCount = 0;
$column = 0;
for ($i = 0; $i <= count($input) - 1; $i++) {
    if ($input[$i][$column] == '#') {
        $treeCount++;
    }
    $column += 3;
    if ($column > (strlen($input[$i]) - 1)) {
        $column -= strlen($input[$i]);
    }
}

var_dump($treeCount);
die;