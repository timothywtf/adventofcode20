<?php
include_once '../../vendor/autoload.php';
$input = file_get_contents(__DIR__ . "/../input/20.txt");

$tiles = preg_split("#\n\s*\n#Uis", trim($input));
$tileRegister = [];
foreach ($tiles as $tile) {
    $tileRows = explode(chr(10), $tile);
    $firstRow = array_shift($tileRows);
    preg_match("/^Tile (?<id>\d*):$/", $firstRow, $matches);
    $tileData = [];
    foreach ($tileRows as $tileRow) {
        $tileData[] = str_split($tileRow);
    }
    $matrix = new \App\Library\Matrix($tileData);
    $matrix->setId($matches['id']);
    $tileRegister[] = $matrix;
}

$range = sqrt(count($tileRegister));
$picture = [];
$copyTiles = $tileRegister;

for ($i = 0; $i < count($copyTiles); $i++) {
    var_dump($i);
    $picture[0][0] = $copyTiles[$i];
    unset($copyTiles[$i]);
    for ($y = 0; $y < $range; $y++) {
        for ($x = 0; $x < $range; $x++) {
            if (isset($picture[$y][$x])) {
                continue;
            }
            /** @var \App\Library\Matrix|null $toTheLeft */
            $toTheLeft = $picture[$y][$x-1] ?? null;
            /** @var \App\Library\Matrix|null $toTheTop */
            $toTheTop = $picture[$y-1][$x] ?? null;
            /** @var \App\Library\Matrix $copyTile */
            foreach ($copyTiles as $copyKey => $copyTile) {
                foreach ($copyTile->getTileOptions() as $tileOption) {
                    $matchLeft = ($toTheLeft == null || ($toTheLeft instanceof \App\Library\Matrix && $tileOption->getEdge('W') == $toTheLeft->getEdge('E')));
                    $matchTop = ($toTheTop == null || ($toTheTop instanceof \App\Library\Matrix && $tileOption->getEdge('N') == $toTheTop->getEdge('S')));

                    if ($matchLeft && $matchTop) {
                        $picture[$y][$x] = $tileOption;
                        if (count($picture) == count($tileRegister)) {
                            break 5;
                        }
                        unset($copyTiles[$copyKey]);
                        continue 3;
                    }
                }

            }
            $copyTiles = $tileRegister;
            $picture = [];
            break 2;
        }
    }
}

$nw = $picture[0][0]->getId(true);
$ne = $picture[0][$range-1]->getId(true);
$se = $picture[$range-1][$range-1]->getId(true);
$sw = $picture[$range-1][0]->getId(true);
var_dump($nw*$ne*$se*$sw);