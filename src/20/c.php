<?php
include_once '../../vendor/autoload.php';
$input = file_get_contents(__DIR__ . "/../input/frameless_input.txt");

$rows = explode(chr(10), $input);
$waveCount = substr_count($input, '#');
$range = count($rows);
foreach ($rows as $columns) {
    $pictureWithoutFrame[] = str_split($columns);
}
$pictureWithoutFrameMatrix = new \App\Library\Matrix($pictureWithoutFrame);

$toCheck = [
    [+1, +1],
    [+4, +1],
    [+7, +1],
    [+10, +1],
    [+13, +1],
    [+16, +1],
    [+5, +0],
    [+6, +0],
    [+11, +0],
    [+12, +0],
    [+17, +0],
    [+18, +0],
    [+19, +0],
    [+18, -1],
];

$nessiesSpotted = 0;

/** @var \App\Library\Matrix $pictureOrientations */
foreach ($pictureWithoutFrameMatrix->getTileOptions() as $pictureOrientations) {
    if ($nessiesSpotted != 0) {
        // no need to check further
        break;
    }
    $grid = $pictureOrientations->getGrid();
    for ($y = 0; $y < $range; $y++) {
        for ($x = 0; $x < $range; $x++) {
            if ($grid[$y][$x] != '#') {
                continue;
            }
            $nessieParts = 0;
            foreach ($toCheck as $checkItem) {
                $check = $grid[$y+$checkItem[1]][$x+$checkItem[0]] ?? null;
                if ($check == '#') {
                    $nessieParts++;
                }
            }
            if ($nessieParts == 14) {
                $nessiesSpotted++;
            }
        }
    }
}

$roughness = $waveCount - (15 * $nessiesSpotted);
var_dump("Roughness: " . $roughness);