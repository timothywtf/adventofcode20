<?php
$input = file_get_contents(__DIR__ . "/../input/21.txt");
$foods = explode(chr(10), $input);

$rules = [];
foreach ($foods as $food) {
    preg_match("/^(?<ingredients>.*)\(contains\s(?<allergens>.*)\)$/", $food, $matches);
    $rules[] = [
        'allergens' => explode(", ", $matches['allergens']),
        'ingredients' => explode(" ", trim($matches['ingredients']))
    ];
}

usort($rules, function ($a, $b) {
    return count($a['allergens']) <=> count($b['allergens']);
});

$allergens = [];
foreach ($rules as $rule) {
    $allergens = array_merge($allergens, $rule['allergens']);
}
$allergens = array_unique($allergens);
$allergensFound = [];

while (count($allergensFound) < count($allergens)) {
    foreach ($allergens as $allergen) {
        $allergenIngredient = array_filter($rules, function ($rule) use ($allergen) {
            return in_array($allergen, $rule['allergens']);
        });

        $allergenIngredients = [];
        foreach ($allergenIngredient as $item) {
            $allergenIngredients[] = $item['ingredients'];
        }

        if (count($allergenIngredients) > 1) {
            $commonAllergenIngredients = call_user_func_array('array_intersect', $allergenIngredients);
        } elseif (count($allergenIngredients) === 1 && count(reset($allergenIngredients))) {
            $commonAllergenIngredients = reset($allergenIngredients);
        }

        if (count($commonAllergenIngredients) === 1) {
            $commonAllergenIngredient = current($commonAllergenIngredients);

            foreach ($rules as &$rule) {
                if (($commonAllergenIngredientKey = array_search($commonAllergenIngredient, $rule['ingredients'])) !== false) {
                    unset($rule['ingredients'][$commonAllergenIngredientKey]);
                }
                if (($commonAllergenAllergenKey = array_search($allergen, $rule['allergens'])) !== false) {
                    unset($rule['allergens'][$commonAllergenAllergenKey]);
                }
            }

            if (!isset($allergensFound[$allergen])) {
                $allergensFound[$allergen] = $commonAllergenIngredient;
            }
        }
    }
}

ksort($allergensFound);
var_dump("canonical dangerous ingredient list: " . implode(",", $allergensFound));
