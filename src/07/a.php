<?php
include_once '../../vendor/autoload.php';
$input = explode(chr(10), file_get_contents(__DIR__ . '/../input/07.txt'));

$all = [];
foreach ($input as $item) {
    if (preg_match("/.*no other bags./", $item)) {
        continue;
    }
    if (preg_match("/(?<parent>.*) bags contain (?<children_concat>.*)./", $item, $matches)) {
        $parent = $matches['parent'];
        $bagsString = explode(",", $matches['children_concat']);
        $bags = [];
        foreach ($bagsString as $bagString) {
            if (preg_match("/(?<amount>\d)* (?<color>.*) bag(s)?/", trim($bagString), $bagMatches)) {
                $bag['amount'] = $bagMatches['amount'];
                $bag['color'] = $bagMatches['color'];
                $bags[] = $bag;
            }
        }
    }
    $all[$parent] = [
        'name' => $parent,
        'children' => $bags
    ];
}

$graph = new Fhaculty\Graph\Graph();
$getVertex = function ($name) use ($graph) {
    try {
        return $graph->getVertex($name);
    } catch (Exception $e) {
        return $graph->createVertex($name);
    }
};

foreach ($all as $bag) {
    $parent = $getVertex($bag['name']);
    foreach ($bag['children'] as $childName) {
        $child = $getVertex($childName['color']);
        $parent->createEdgeTo($child);
    }
}


$graphviz = new Graphp\GraphViz\GraphViz();
$graphviz->display($graph);
die;

$shinyGoldBag = $graph->getVertex('shiny gold');

$breathFirst = new \Graphp\Algorithms\Search\BreadthFirst($shinyGoldBag);
$breathFirst->setDirection(\Graphp\Algorithms\Search\Base::DIRECTION_REVERSE);
$vertices = [];
foreach ($breathFirst->getVertices() as $item) {
    $vertices[] = ($item != $shinyGoldBag)
        ? $item
        : null;
}

dd(count(array_filter($vertices)));
