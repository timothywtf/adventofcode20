<?php
$input = explode(chr(10), file_get_contents(__DIR__ . '/../input/07.txt'));
foreach ($input as $item) {
    if (preg_match("/.*no other bags./", $item)) {
        continue;
    }
    if (preg_match("/(?<parent>.*) bags contain (?<children_concat>.*)./", $item, $matches)) {
        $parent = $matches['parent'];
        $bagsString = explode(",", $matches['children_concat']);
        $bags = [];
        foreach ($bagsString as $bagString) {
            if (preg_match("/(?<amount>\d)* (?<color>.*) bag(s)?/", trim($bagString), $bagMatches)) {
                $bag['amount'] = $bagMatches['amount'];
                $bag['color'] = $bagMatches['color'];
                $bags[] = $bag;
            }
        }
    }
    $all[$parent] = [
        'name' => $parent,
        'children' => $bags
    ];
}

$queue = ['shiny gold'];
$bagsCounted = 0;
while (!empty($queue)) {
    $item = array_shift($queue);
    $bagsCounted++;
    if (isset($all[$item])) {
        $children = $all[$item]['children'];

        foreach ($children as $child) {
            for ($i = 1; $i <= $child['amount']; $i++) {
                $queue[] = $child['color'];
            }
        }

    }
}

var_dump($bagsCounted-1);