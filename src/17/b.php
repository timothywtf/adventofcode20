<?php
$input = file_get_contents(__DIR__ . '/../input/17.txt');
$input = explode(chr(10), $input);

$space = 1;
$rangeX = [0,0];
$rangeY = [0,0];
$rangeZ = [0,0];
$rangeW = [0,0];

$states = [];
$beginState = [];


foreach ($input as $rowIndex => $row) {
    $cells = str_split($row);
    foreach ($cells as $cellIndex => $cell) {
        $beginState[0][0][$rowIndex][$cellIndex] = ($cell == '#');
    }
}

$states[] = $beginState;
setRange($beginState, $rangeW, $rangeZ, $rangeY, $rangeX);

function setRange($state, &$rangeW, &$rangeZ, &$rangeY, &$rangeX)
{
    $rangeW[0] = min(array_merge(array_keys($state), [$rangeW[0]]));
    $rangeW[1] = max(array_merge(array_keys($state), [$rangeW[1]]));
    foreach ($state as $wItems) {
        $rangeZ[0] = min(array_merge(array_keys($wItems), [$rangeZ[0]]));
        $rangeZ[1] = max(array_merge(array_keys($wItems), [$rangeZ[1]]));
        foreach ($wItems as $zItems) {
            $rangeY[0] = min(array_merge(array_keys($zItems), [$rangeY[0]]));
            $rangeY[1] = max(array_merge(array_keys($zItems), [$rangeY[1]]));
            foreach ($zItems as $yItems) {
                $rangeX[0] = min(array_merge(array_keys($yItems), [$rangeX[0]]));
                $rangeX[1] = max(array_merge(array_keys($yItems), [$rangeX[1]]));
            }
        }
    }

}

function printState($state, $highlight = array())
{
    foreach ($state as $dimensionKey => $cube) {
        foreach ($cube as $sliceKey => $slice) {
            echo sprintf("z=%s w=%s", $sliceKey, $dimensionKey) . PHP_EOL;
            foreach ($slice as $yKey => $row) {
                foreach ($row as $xKey => $cell) {
                    $value = $cell ? "#" : ".";
                    if (!empty($highlight) && $highlight[0] == $sliceKey && $yKey == $highlight[1] && $xKey == $highlight[2]) {
                        $value = sprintf("\e[4m%s\e[0m", $value);
                    }
                    echo $value;
                }
                echo PHP_EOL;
            }
            echo PHP_EOL;
        }
    }
}

function dump ($w, $z, $y, $x) {
    var_dump("(" . $w . "," . $z . "," .  $y . "," . $x . ")" . PHP_EOL);
}

function countState($state, $c)
{
    $count = 0;
    foreach ($state as $wItems) {
        foreach ($wItems as $zItems) {
            foreach ($zItems as $yItems) {
                foreach ($yItems as $xItems) {
                    if ($xItems) {
                        $count++;
                    }
                }
            }
        }
    }
    var_dump(sprintf("Cycle %s: %s", $c, $count));
}

function countActiveBorderCells($cw, $cz, $cy, $cx, $state)
{
    $active = 0;
    for ($w = $cw - 1; $w <= $cw + 1; $w++) {
        for ($z = $cz - 1; $z <= $cz + 1; $z++) {
            for ($y = $cy - 1; $y <= $cy + 1; $y++) {
                for ($x = $cx - 1; $x <= $cx + 1; $x++) {
                    if ($w == $cw && $z == $cz && $y == $cy && $x == $cx) {
                        continue;
                    }
                    if (!isset($state[$w][$z][$y][$x])) {
                        continue;
                    }
                    if ($state[$w][$z][$y][$x] == true) {
                        $active++;
                    }
                }
            }
        }
    }
    return $active;
}



$cycleUntil = 6;
for ($c = 1; $c <= $cycleUntil; $c++) {
    //set current state
    $currentState = $states[count($states) - 1];
    //determine bounds
    printState($currentState, [0, 2, 1]);
    $newState = [];

    for ($w = $rangeW[0] - 1; $w <= $rangeW[1] + 1; $w++) {

        for ($z = $rangeZ[0] - 1; $z <= $rangeZ[1] + 1; $z++) {

            for ($y = $rangeY[0] - 1; $y <= $rangeY[1] + 1; $y++) {

                for ($x = $rangeX[0] - 1; $x <= $rangeX[1] + 1; $x++) {
                    //evaluate border
                    $currentCell = (isset($currentState[$w][$z][$y][$x])) ? $currentState[$w][$z][$y][$x] : false;

                    $currentBorderCellCount = countActiveBorderCells($w, $z, $y, $x, $currentState);

                    $newState[$w][$z][$y][$x] = false;


                    if ($currentCell) {
                        // active
                        if ($currentBorderCellCount == 2 || $currentBorderCellCount == 3) {
                            $newState[$w][$z][$y][$x] = true;
                        }
                    } else {
                        // inactive
                        if ($currentBorderCellCount == 3) {
                            $newState[$w][$z][$y][$x] = true;
                        }
                    }
                }
            }
        }
    }

    printState($newState, [0, 2, 1]);
    setRange($newState, $rangeW, $rangeZ, $rangeY, $rangeX);
    countState($newState, $c);
    $states[] = $newState;
}

