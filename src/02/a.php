<?php
$input = explode(chr(10), file_get_contents(__DIR__ . '/../input/02.txt'));

$validCount = 0;
foreach ($input as $item) {
    if (preg_match('/(?<min>\d{1,3})-(?<max>\d{1,3}) (?<letter>[a-z]{1}): (?<password>.*)/', $item, $matches)) {
        $substringCount = substr_count($matches['password'], $matches['letter']);
        if ($substringCount >= (int)$matches['min'] && $substringCount <= (int)$matches['max']) {
            $validCount++;
        }
    }
}
var_dump($validCount);
