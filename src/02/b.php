<?php
$input = explode(chr(10), file_get_contents(__DIR__ . '/../input/02.txt'));

$validCount = 0;
foreach ($input as $item) {
    if (preg_match('/(?<first>\d{1,3})-(?<second>\d{1,3}) (?<letter>[a-z]{1}): (?<password>.*)/', $item, $matches)) {
        $letterMatch = 0;
        if ($matches['password'][(int)$matches['first']-1] == $matches['letter']) {
            $letterMatch++;
        }
        if ($matches['password'][(int)$matches['second']-1] == $matches['letter']) {
            $letterMatch++;
        }
        if ($letterMatch === 1) {
            $validCount++;
        }
    }
}
var_dump($validCount);
