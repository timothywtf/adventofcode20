<?php
$input = explode(chr(10), file_get_contents(__DIR__ . '/../input/05.txt'));

$highestSeatId = null;
foreach ($input as $seat) {

    $calcRow = function ($seat) {
        $min = 0;
        $max = 127;
        for ($i = 1; $i < 8; $i++) {
            $half = $seat[$i-1];
            if ($half == 'B') {
                $min += 128/pow(2,$i);
            } elseif ($half == 'F') {
                $max -= 128/pow(2,$i);
            }
        }
        return $min;
    };

    $calcColumn = function ($seat) {
        $seat = substr($seat, -3, strlen($seat) - 1);
        $min = 0;
        $max = 7;
        for ($i = 1; $i < 4; $i++) {
            $half = $seat[$i-1];
            if ($half == 'R') {
                $min += 8/pow(2,$i);
            } elseif ($half == 'L') {
                $max -= 8/pow(2,$i);
            }
        }

        return $min;
    };

    $calcRowId = function ($seat) use ($calcRow, $calcColumn) {
        return $calcRow($seat) * 8 + $calcColumn($seat);
    };

    $seatId = $calcRowId($seat);
    $highestSeatId = $seatId > $highestSeatId || is_null($highestSeatId)
        ? $seatId
        : $highestSeatId;
}
var_dump($highestSeatId);
