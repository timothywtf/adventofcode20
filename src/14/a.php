<?php
$file_get_contents = file_get_contents(__DIR__ . '/../input/14.txt');
$input = explode(PHP_EOL, $file_get_contents);

$memory = [];
$mask = null;
foreach ($input as $instruction) {
    if (preg_match("/mask = (?<mask>[10X]*)/", $instruction, $matches)) {
        $mask = $matches['mask'];
    } else if (preg_match("/mem\[(?<address>\d*)\] = (?<value>\d*)/", $instruction, $matches)) {
        $address = (int)$matches['address'];
        $value = (int)$matches['value'];

        $memory[$address] = str_pad(decbin($value), 36, 0, STR_PAD_LEFT);
        $splitMask = str_split($mask);
        $splitValue = str_split($memory[$address]);

        for ($i = count($splitMask) - 1; $i >= 0; $i--) {
            if ($splitMask[$i] == '1' || $splitMask[$i] == '0') {
                $splitValue[$i] = $splitMask[$i];
            }
        }
        $test = implode('', $splitValue);
        $memory[$address] = bindec(implode('', $splitValue));
    }
}

var_dump(array_sum($memory));die;
