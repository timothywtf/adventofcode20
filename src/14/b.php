<?php
$file_get_contents = file_get_contents(__DIR__ . '/../input/14.txt');
$input = explode(PHP_EOL, $file_get_contents);

function getCombinations($mask)
{
    $masks = [];
    $splitMask = str_split($mask);
    $replacementMask = $splitMask;
    $nodes = [];
    $keyMap = [];

    foreach ($splitMask as $key => $item) {
        if ($item == 'X') {
            $keyMap[] = $key;
            $nodes[] = [0, 1];
        }
    }
    if (empty($nodes)) {
        return [];
    }

    $combinations = runCombinations($nodes);
    foreach ($combinations as $replacementBits) {
        foreach ($keyMap as $key => $place) {
            $replacementMask[$place] = $replacementBits[$key];
        }
        $masks[] = implode("", $replacementMask);
    }

    return $masks;
}

function runCombinations($arrays) {
    $result = array(array());
    foreach ($arrays as $property => $property_values) {
        $tmp = array();
        foreach ($result as $result_item) {
            foreach ($property_values as $property_value) {
                $tmp[] = array_merge($result_item, array($property => $property_value));
            }
        }
        $result = $tmp;
    }
    return $result;
}

function applyMask($value, $mask)
{
    $binValue = str_pad(decbin($value), 36, 0, STR_PAD_LEFT);
    $splitMask = str_split($mask);
    $splitValue = str_split($binValue);

    for ($i = count($splitMask) - 1; $i >= 0; $i--) {
        if ($splitMask[$i] == '1' || $splitMask[$i] == 'X') {
            $splitValue[$i] = $splitMask[$i];
        }
    }
    return implode('', $splitValue);
}

$currentMask = null;
$memory = [];
foreach ($input as $instruction) {
    if (preg_match("/mask = (?<mask>[10X]*)/", $instruction, $matches)) {
        $currentMask = $matches['mask'];
    } else if (preg_match("/mem\[(?<address>\d*)\] = (?<value>\d*)/", $instruction, $matches)) {
        $address = applyMask((int)$matches['address'], $currentMask);
        $maskCombinations = getCombinations($address);
        foreach ($maskCombinations as $maskCombination) {
            $memory[bindec($maskCombination)] = (int)$matches['value'];
        }
    }
}

var_dump(array_sum($memory));
